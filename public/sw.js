self.addEventListener("install", async () => {
  const cache = await caches.open("timer-lj");
  // it stores all resources on first SW install
  cache.addAll(["/", "index.html"]); 
});
 
self.addEventListener("fetch", event => {
   event.respondWith(
     caches.match(event.request).then(cachedResponse => {
         const networkFetch = fetch(event.request)
           .then(response => {
             // update the cache with a clone of the network response
             caches.open("timer-lj").then(cache => {
                 cache.put(event.request, response.clone());
             });
           })
           .catch(console.log);
         // prioritize cached response over network
         return cachedResponse || networkFetch;
     }
   )
  )
});

self.addEventListener('activate', function() {
	console.log('ACTIVATE sw on timer lesjanvier');
});

self.addEventListener('sync', function() {
	console.log('SYNC sw on timer lesjanvier');
});
