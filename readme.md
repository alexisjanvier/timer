# Timer

Une simple application web, installable, permettant de compter des séries d'exercices et les temps de pose entre chacune quand on fait du sport.

![screncap](screen.png)
